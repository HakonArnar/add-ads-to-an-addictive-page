document.body.innerHTML = `<div id="main"> <div id="ad"></div> </div>`

var { addHeader, addImg, addAd, renderAd } = require('../main.js')

test('The news item should contain a <h2> header', function (){
    var testHTML = function() {
        renderAd()
        return document.body.innerHTML
    }
    expect( testHTML() ).toMatch(/<h2>/);
});

test('The news item should contain an <img> element', function (){
    var testHTML = function() {
        renderAd()
        return document.body.innerHTML
    }
    expect( testHTML() ).toMatch(/<img/);
});

test('The news item should contain a <p> element', function (){
    var testHTML = function() {
        renderAd()
        return document.body.innerHTML
    }
    expect( testHTML() ).toMatch(/<p>/);
});
